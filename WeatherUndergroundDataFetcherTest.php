<?php
/*
	Simple test for WeatherUndergroundDataFetcher.

	One of the tests uses a mock for WeatherUndergroundDataFetcher and a test double
	is used for buildWURequestURL, which is set to return a local json file.
	This has numerous benfits:

		- the real WU server is not being hit so it's much quicker
		- this allows for mutiple flavors of json files to be used in testing 
		- using fixed json files removes the variable of the live server
*/

include "WeatherUndergroundDataFetcher.php";

class WeatherUndergroundDataFetcherTest extends PHPUnit_Framework_TestCase
{
	// Actually hit the WU server.  Ensure it returns a map with what we requested.
	// It is much more preferable to use a mock so the request is not sent to the server.
	// See below.
	public function testValid()
	{
		$wDF = new WeatherUndergroundDataFetcher();
		$wDF->initialize();
		$wDF->addField(WeatherUndergroundDataFetcher::CURRENT_TEMP_F_FIELD);
		$wDF->location("/TX/Austin");
		$temp = $wDF->getWeatherData();

		$this->assertArrayHasKey(WeatherUndergroundDataFetcher::CURRENT_TEMP_F_FIELD, $temp);   
	}


	public function testValidWithMock()
	{
		// Create a mock for WeatherUndergroundDataFetcher.  Ue a test double for buildWURequestURL() to 
		// return the name of a json file.
		$wDF = $this->getMockBuilder('WeatherUndergroundDataFetcher')
						 ->setMethods(array('buildWURequestURL'))
						 ->getMock();

		$wDF->method('buildWURequestURL')
			 ->will($this->returnValue("valid1.json"));

		$wDF->initialize();
		$wDF->addField(WeatherUndergroundDataFetcher::CURRENT_TEMP_F_FIELD);
		$wDF->addField(WeatherUndergroundDataFetcher::CURRENT_REL_HUM_FIELD);
		$wDF->location("/TX/Austin");
		$returnedWeatherData = $wDF->getWeatherData();

		// Ensure the fields we want are in the map.
		$this->assertArrayHasKey(WeatherUndergroundDataFetcher::CURRENT_TEMP_F_FIELD, $returnedWeatherData);   
		$this->assertArrayHasKey(WeatherUndergroundDataFetcher::CURRENT_REL_HUM_FIELD, $returnedWeatherData);

		// Check the size.
		$this->assertEquals(2, count($returnedWeatherData));
	}



	public function testException()
	{
		$wDF = new WeatherUndergroundDataFetcher();
		$wDF->initialize();

		$this->setExpectedException('Exception');
		$wDF->addField("BadField");
	}

}
?>