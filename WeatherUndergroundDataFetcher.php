<?php

/**
	A simple wrapper for the WeatherUnderground API.  See http://www.wunderground.com/weather/api/d/docs.
	Created by Gary Ormsby
 */
class WeatherUndergroundDataFetcher {

	CONST URL_SEPARATOR = '/';
	CONST CURRENT_TEMP_F_FIELD = 'CurrentTempInFarenheit';
	CONST CURRENT_REL_HUM_FIELD = 'CurrentRelativeHumidity';

	private $key;
	private $serviceURL;
	private $format;

	private $retrievableWeatherData =
						[
						WeatherUndergroundDataFetcher::CURRENT_TEMP_F_FIELD => 
							[	'sectionName' => 'current_observation',
								'fieldName' => 'temp_f',
							],

						WeatherUndergroundDataFetcher::CURRENT_REL_HUM_FIELD => 
							[	'sectionName' => 'current_observation',
								'fieldName' => 'relative_humidity',
							],
						// More can be added.
						];

	// An array of the weather data fields that are being requested.
	private $weatherDataToRetrieve = [];

	/**
	 * Set what we need. 
	 */
	public function initialize() {
		// My personal Key
		$this->key = '828d813414a90acc';
  		$this->serviceURL = 'api.wunderground.com/api/';

  		/// The other option is xml.
  		$this->format = ".json";
	}

	/**
	 * Queue up the weaather data fields.
	 * @param string The weather data field to queue up.
	 * @throws Exception
	 */
	public function addField($weatherField) {

		if (!array_key_exists($weatherField, $this->retrievableWeatherData)) {
			throw new Exception("This field is not supported.");
		}


		array_push($this->weatherDataToRetrieve, $weatherField);
	}

	/**
	 * Save the location 
	 * @param string The location we're interested in.
	 */
	public function location($location) {
		$this->location = $location;
	}

	/**
	 * Gets the requested data from WeatherGround and returns it in a hash map.
	 * @return array A map of the data requested.
	 */
	public function getWeatherData() {
		$jsonWeatherData = file_get_contents($this->buildWURequestURL());
		return $this->getFieldsFromReturnedData($this->weatherDataToRetrieve, json_decode($jsonWeatherData));

	}

	/**
	 * Return the value of the passed key for the requested weather data field.
	 * @param string The weather data field.
	 * @param string The key to lookup.
	 * @return string
	 */
	private function lookupValue($weatherDataField, $key) {
		return $this->retrievableWeatherData[$weatherDataField][$key];		
	}

	/**
	 * Build the URL to get the requested weather data.
	 * @return string
	 */
	private function buildWURequestURL() {
		$url = "http://" . $this->serviceURL . WeatherUndergroundDataFetcher::URL_SEPARATOR .
						$this->key . WeatherUndergroundDataFetcher::URL_SEPARATOR .
						"geolookup" . WeatherUndergroundDataFetcher::URL_SEPARATOR .
						"conditions"  . WeatherUndergroundDataFetcher::URL_SEPARATOR .
						"q" . WeatherUndergroundDataFetcher::URL_SEPARATOR .
						$this->location . $this->format;
		return $url;
	}


	/**
	 * Parse the string returned from WeatherUnderground.  Put the data for the 
	 * requested fields in a map. 
	 * @param array A list of the fields we're interested in.
	 * @param mixed A json decoded string returned from Weather Underground.
	 * @return array A map of the data requested.
	 */
	private function getFieldsFromReturnedData($weatherFieldsToRetrieve, $parsed_json) {
		$mapOfWeatherDataToReturn = [];
		foreach ($weatherFieldsToRetrieve as $weatherField) {
			$section = $this->lookupValue($weatherField, 'sectionName');
			$field = $this->lookupValue($weatherField, 'fieldName');
			$mapOfWeatherDataToReturn[$weatherField] = $parsed_json->{$section}->{$field};
		}
		return $mapOfWeatherDataToReturn;

	}


}


