<?php

include "WeatherUndergroundDataFetcher.php";

$wDF = new WeatherUndergroundDataFetcher();
$wDF->initialize();
$wDF->addField(WeatherUndergroundDataFetcher::CURRENT_TEMP_F_FIELD);
$wDF->addField(WeatherUndergroundDataFetcher::CURRENT_REL_HUM_FIELD);
$state = 'TX';
$city = 'Austin';
$wDF->location($state . '/' . $city );
$dataMap = $wDF->getWeatherData();
$tempF = $dataMap[WeatherUndergroundDataFetcher::CURRENT_TEMP_F_FIELD];


print ("Current temperature in $city is " . $dataMap[WeatherUndergroundDataFetcher::CURRENT_TEMP_F_FIELD] . " degrees farenheit.\n");    	
print ("Current relative humidity in $city is " . $dataMap[WeatherUndergroundDataFetcher::CURRENT_REL_HUM_FIELD] . ".\n");    	
